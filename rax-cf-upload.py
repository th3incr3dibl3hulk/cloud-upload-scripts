import os
print("Gathering available containers.")
os.system("rack files container list")
container = input("To which container should your file be uploaded?\n")
print()
file_name = input("What would you like to name your upload?  Please use the regular name of the file, not the encrypted or compressed name.\n")
print()
print("Upload beginning.  5GB chunks selected by default.")


os.system('rack files large-object upload --container {0} --size-pieces 5000 --name {1} --file encrypted-{1}.tar.gz.gpg'.format(container,file_name))

print("Upload complete.")
