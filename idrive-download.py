import os

# Print a message indicating the beginning of the process
print("Locating all files in media backup")

# Execute AWS CLI command to list all files in the specified S3 bucket and directory
os.system("aws s3 ls s3://media-server-backup-offsite-6141/ --recursive")

# Prompt the user to input the name of the movie they want to download
print("Which file would you like to download? Use only the movie name, ignoring the encrypted prefix")
movie_target = input()

# Print a blank line for formatting
print()

# Print a message indicating the beginning of the download for the specified movie
print("Download beginning for {}.".format(movie_target))

# Use AWS CLI to download the specified movie file from the S3 bucket to the local directory
os.system('aws s3 cp s3://media-server-backup-offsite-6141/encrypted-{0}.tar.gz.gpg /home/deadpool/idrive_temp'.format(movie_target))

# Print a message indicating that the download is complete
print("Download complete.")
