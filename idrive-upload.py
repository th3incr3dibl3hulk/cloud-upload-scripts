import os

# Print a message indicating the beginning of the process
print("Gathering available buckets.")

# Execute AWS CLI command to list all S3 buckets
os.system("aws s3api list-buckets")

# Prompt the user to input the name of the bucket where they want to upload the file
bucket_name = input("To which bucket should your file be uploaded?\n")

# Print a blank line for formatting
print()

# Prompt the user to input the name they want to give to the uploaded file
file_name = input("What would you like to name your upload?  Please use the regular name of the file, not the encrypted or compressed name.\n")

# Print a blank line for formatting
print()

# Print a message indicating the beginning of the upload process
print("Upload beginning.")

# Use AWS CLI to upload the specified file to the specified S3 bucket
os.system('aws s3 cp encrypted-{0}.tar.gz.gpg s3://{1}/{0}'.format(file_name, bucket_name))

# Print a message indicating that the upload is complete
print("Upload complete.")
